# Sujet 5 - Prévoir la pollution avec des données météo

Il s’agit de prévoir la concentration en polluants dans différentes villes françaises à partir de données météorologiques (vent, précipitations, etc.).

Les données météorologiques sont issues du [Climate Data Store](https://cds.climate.copernicus.eu/cdsapp#!/home) du programme Copernicus.

La variable à prédire est la concentration en particules fines dites [PM<sub>2.5</sub>](https://www.airparif.asso.fr/les-particules-fines), c'est-à-dire dont le diamètre est inférieur à 2.5 μm. Ces particules sont néfastes pour la santé.

Le problème peut être traité en appliquant des algorithmes de machine learning supervisé tels que la régression logistique, les forêts aléatoires, les Gradient Boosted Trees ou encore les réseaux de neurones.

Les notebooks ML_meteo_Python.ipynb et ML_meteo_R.ipynb peuvent être ouverts depuis un service Jupyter du datalab, tandis que ML_meteo_R.Rmd peut être ouvert depuis un service RStudio.
