# Détecter la couverture du Sol à partir d'images satellites :

Pour ce sujet, on vous propose d'entraîner uin algorithme de segmentation sémantique permettant d'attribuer à chaque point d'une image une catégorie.
On appliquera cet algorithme à des images satellites.

Dans un premier temps on vous propose d'appréhender le problème plus simple de la classification d'image, ici des images d'oiseau, en utilisant des algorithmes de type réseau de neurone convolutifs (En R ou en Python).

Cette phase pédagogique vous permettra d'appréhender les outils de bases et d'aborder plus facilement la suite. 

## Pour Commencer :

Pour l'exercice de Classification d'oiseau un notebook est disponible en **R** et en Python, les objets et fonctions associées au deep learning sont très similaires dans les deux langages. La suite sur les images satellites sera proposée en Python uniquement.

**Pour le notebook en R :** 

- Lancer un service Rstudio 
- Dans le terminal du service, executer : *git clone https://git.lab.sspcloud.fr/ssplab/2022-06-funathon/sujet-9-detecter-la-couverture-du-sol-a-partir-d-images-satellites.git* et ouvrir le fichier Classification_oiseau_avec_R.Rmd


**Pour les notebook Tensorflow**, lancer un service **Tensorflow** sur Onyxia **avec les configurations suivantes** : 

- Dans Ressources, demander un GPU Nvidia.com/gpu, 1 cpu et 8 Mo de memory
- Dans Persistency demander 30 giga en Persistent volume size

Si le service se lance en moins d'une minute vous pouvez vous y connecter

**Dans le cas contraire** : Toutes les ressources en GPU ont déjà été demandées, ainsi le le service ne se lancera pas (premier arrivé premier servi).

Dans ce cas recommencer le lancement en demandant **0** GPU Nvidia.com/gpu. L'absence de GPU n'est pas bloquante pour s'amuser sur ce projet !

Une fois le service lancé, sur Tensorflow ouvrir un terminal sur Onyxia et taper :

*cd work* 

Puis :
*git clone https://git.lab.sspcloud.fr/ssplab/2022-06-funathon/sujet-9-detecter-la-couverture-du-sol-a-partir-d-images-satellites.git*

Vous pouvez enfin ouvrir le premier notebook "Classification_oiseau.ipynb" et commencer à vous amuser :)

Une fois ce notebook terminé vous pouvez passer à "Donnees_satellites.ipynb" 

