# Sujet 6 : Quel sera l'impact de la montée des eaux ?

Remarque : aucune donnée préalable n'est nécessaire, elles sont toutes téléchargées pendant le sujet.

L'objectif du sujet est de travailler sur un appariement spatial entre les données carroyées de l'Insee (qui fournissent beaucoup d'informations, notamment, mais pas seulement, sur le nombre d'habitants dans des carreaux de petites tailles) et les données de l'IGN sur l'altitude.

Il est à noter cependant que ce travail suppose un peu de manipulation de fichier (effacer, copier, dézipper) qui est traité ici par des lignes de code, histoire de profiter de l'occasion pour apprendre à le faire si vous n'en avez pas encore eu l'occasion.

Pour traiter le sujet, deux démarches selon le langage :

## Sous R

Sous R, l'idée est d'ouvrir le "R_Notebook_complet.Rmd". Il s'agit d'un fichier RMarkDown.

Ce fichier présente le cheminement complet du sujet. En l'ouvrant dans RStudio, il suffit de le suivre dans l'ordre. Chaque fois qu'un travail est proposé, un espace pour remplir la réponse vous est proposé, de ce type :

```{r}
# A vous : télécharger les données sur le site de l'IGN
```

Chaque fois, l'idée est alors de remplir avec du code R, puis de lancer le contenu soit en cliquant sur la petite flèche à droite, soit en sélectionnant le code puis en appuyant sur Ctrl+Entrée. 

Une proposition de solution est en général présente dans la cellule qui suit. Libre à vous de la suivre, de l'executer... ou non ! Notez que ce n'est qu'une proposition de solution, pas nécessairement la solution optimale au problème.

Alternativement, vous pouvez bien sûr vous inspirer du RMarkDown et créer un document de type .R dans lequel vous collez vos réponses. Ou créer votre propre RMarkDown. 

## Sous Python

Sous Python, le sujet est proposé sous la forme d'un notebook Jupyter. L'idée est alors simplement d'ouvrir le notebook, et de le suivre ! :)
