# Sujet 7 : Classer les réponses du Grand Débat dans des catégories prédéfinies

Au cours du premier trimestre 2019 s'est tenu un "grand débat national" en France. Quatre thèmes étaient proposés par le gouvernement : transition écologique, fiscalité et dépenses publiques, démocratie et citoyenneté, organisation de l'état et des services publics. Ce débat a pris plusieurs formes, mais l'une d'elle était la possibilité de donner son avis via l'utilisation d'un formulaire Internet. Une contribution consiste en une succession de réponses à des questions ouvertes et fermées sur l'une des quatre thématiques proposées.

Ce sujet consiste à classer les contributions dans des catégories prédéfinies. Deux tâches sont proposées :
- reclasser l'ensemble des contributions dans les 4 grands thèmes à partir du texte des réponses
- classer les réponses à une question en lien avec l'environnement dans des domaines préétablis

Les données sont issues de deux sources : 
- Le [site du Grand Débat National](https://granddebat.fr/pages/donnees-ouvertes) permet de télécharger l'ensemble des contributions. Il est également possible de les consulter directement sur le site, [ici](https://granddebat.fr/pages/consulter-les-propositions).
- L'initiative [La Grande Annotation](https://grandeannotation.fr/) permet d'obtenir pour la plupart des questions ouvertes du Grand Débat des réponses classées dans des catégories. Plus de 1000 personnes ont lu et annoté des contributions pour les classer.

L'approche proposée ici est celle du _word embedding_. Elle consiste à associer à chaque mot un vecteur numérique (pour le rendre compréhensible par une machine), de façon à ce que deux mots proches sémantiquement possèdent des vecteurs proches numériquement. Pour ce faire, nous allons utiliser la librairie fastText.

Seul un notebook Python est disponible pour ce sujet. Cependant, même sans être familier avec Python, il reste possible et intéressant de parcourir le notebook, voire de lancer un service Jupyter afin d'exécuter les cellules une à une.
