import sys
import os
import hvac
from twarc import Twarc2
import argparse
import json


class TweetFetcher():
    """
    TweetFetcher class.
    """
    
    def __init__(self, secret_name: str):
        """
        Constructor.
        """
        self.secret_name = secret_name
        
    def get_twarc_instance(self):
        """
        Returns a twarc client using credentials in the fetcher's Vault secret.
        """
        client = hvac.Client(url='https://vault.lab.sspcloud.fr',
                             token=os.environ['VAULT_TOKEN'])
        secret = os.environ['VAULT_MOUNT'] + os.environ['VAULT_TOP_DIR'] + '/' + self.secret_name
        mount_point, secret_path = secret.split('/', 1)
        
        secret_dict = client.secrets.kv.read_secret_version(path=secret_path, mount_point = mount_point)
        TWITTER_TOKEN = secret_dict['data']['data']['TOKEN']
        TWITTER_KEY = secret_dict['data']['data']['TWITTER_KEY']
        TWITTER_KEY_SECRET = secret_dict['data']['data']['TWITTER_KEY_SECRET']
        
        return Twarc2(consumer_key=TWITTER_KEY, consumer_secret=TWITTER_KEY_SECRET, bearer_token=TWITTER_TOKEN)
        
    def hydrate_file(self, file_number: int) -> None:
        """
        Hydrates tweets and saves into json file.
        """
        t_inst = self.get_twarc_instance()
        
        with open('../data/changement_climatique_tweets/climate_id.txt.0' + str(file_number)) as f:
            lines = f.read().splitlines()
            
        sample_tweets = t_inst.tweet_lookup(lines)

        tweet_list = []
        for batch_number, batch in enumerate(sample_tweets):
            tweet_data = batch['data']
            user_data = batch['includes']['users']

            user_dict = {user['id']: user for user in user_data}

            for tweet in tweet_data:
                _id = tweet.pop('id')
                try:
                    user_info = user_dict[tweet['author_id']]
                    del user_info['id']
                    tweet['author'] = user_info
                except:
                    pass
                tweet_list.append({'_id': _id, '_source': tweet})
            
            if batch_number % 1000 == 0:
                with open('../tweets_' + str(file_number) + '.json', 'w') as f:
                    json.dump(tweet_list, f)
        print('All data has been dumped.')
        return


def main(secret_name: str, file_number: int):
    """
    Main method.
    """
    fetcher = TweetFetcher(secret_name)
    fetcher.hydrate_file(file_number)
    

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--secret")
    parser.add_argument("--file")
    args = parser.parse_args()
    
    main(args.secret, args.file)
