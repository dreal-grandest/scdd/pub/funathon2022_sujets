"""
Utils file.
"""

def es_iterate_all_documents(es, index, pagesize=250, scroll_timeout="1m", **kwargs):
    """
    Helper to iterate over all values from a single Elasticsearch index.
    Yields a tuple (id, _source) for all the documents.
    """
    is_first = True
    while True:
        # Scroll next
        if is_first: # Initialize scroll
            result = es.search(index=index, scroll="1m", **kwargs, body={
                "size": pagesize
            })
            is_first = False
        else:
            result = es.scroll(body={
                "scroll_id": scroll_id,
                "scroll": scroll_timeout
            })
        scroll_id = result["_scroll_id"]
        hits = result["hits"]["hits"]
        # Stop after no more docs
        if not hits:
            break
        # Yield each entry
        yield from ((hit['_id'], hit['_source']) for hit in hits)
        
def es_iterate_variable(es, index, field_name: str, pagesize=250, scroll_timeout="1m", **kwargs):
    """
    Helper to iterate over all values from a single Elasticsearch index.
    Yields a tuple (id, variable) for all the documents and a field of interest
    """
    generator = es_iterate_all_documents(es, index, pagesize, scroll_timeout, **kwargs)
    for document in generator:
        yield (document[0], document[1][field_name])
        
def create_update_actions_from_generator(generator, target_index_name: str, source_index_name: str):
    """
    Create update actions from a generator.
    """
    for pair in generator:
        _id = pair[0]
        update_id = pair[1]
        header = {"_op_type": "update", "_index": index_name, "_id": _id}
        try:
            update_info = es.get(index=source_index_name, id=update_id)["_source"]
            yield {**header, "doc": update_info}
        except:
            continue
            
def scripted_metric_query(variable_name: str):
    """
    Return a query to count distinct values for a variable.
    """
    return {
        "aggs": {
            "Distinct_Count": {
                "scripted_metric": {
                    "params": {
                        "fieldName": variable_name
                    },
                    "init_script": "state.list = []",
                    "map_script": """
                        if(doc[params.fieldName] != null)
                        state.list.add(doc[params.fieldName].value);
                    """,
                    "combine_script": "return state.list;",
                    "reduce_script": """
                        Map uniqueValueMap = new HashMap(); 
                        int count = 0;
                        for(shardList in states) {
                            if(shardList != null) { 
                                for(key in shardList) {
                                    if(!uniqueValueMap.containsKey(key)) {
                                        count +=1;
                                        uniqueValueMap.put(key, key);
                                    }
                                }
                            }
                        } 
                        return count;
                    """
                }
            }
        }
    }
