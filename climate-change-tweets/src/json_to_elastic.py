from elasticsearch import Elasticsearch
from elasticsearch.helpers import parallel_bulk
from collections import deque


def elastic(host: str):
    """
    Connection avec Elastic sur le data lab.
    """
    es = Elasticsearch([{'host': host, 'port': 9200, 'scheme': 'http'}], http_compress=True, request_timeout=200)
    return es


def main(index: str):
    """
    Main method.
    """
    host = 'elasticsearch-master'
    mappings = {
        'properties': {
            'referenced_tweets': {
                'properties': {
                    'type': {'type': 'keyword'},
                    'id': {'type': 'long'}
                }
            },
            'conversation_id': {'type': 'long'},
            'entities': {
                'properties': {
                    'urls': {
                        'properties': {
                            'start': {'type': 'long'},
                            'end': {'type': 'long'},
                            'url': {'type': 'text'},
                            'expanded_url': {'type': 'text'},
                            'display_url': {'type': 'text'}
                        }
                    },
                    'mentions': {
                        'properties': {
                            'start': {'type': 'long'},
                            'end': {'type': 'long'},
                            'username': {'type': 'text'},
                            'id': {'type': 'long'}
                        }
                    }
                }
            },
            'reply_settings': {'type': 'keyword'},
            'created_at': {'type': 'date'},
            'author_id': {'type': 'long'},
            'possibly_sensitive': {'type': 'boolean'},
            'text': {'type': 'text'},
            'source': {'type': 'keyword'},
            'public_metrics': {
                'properties': {
                    'retweet_count': {'type': 'long'},
                    'reply_count': {'type': 'long'},
                    'like_count': {'type': 'long'},
                    'quote_count': {'type': 'long'} 
                }
            },
            'id': {'type': 'long'},
            'lang': {'type': 'keyword'},
            'author': {
                'properties': {
                    'location': {'type': 'text'},
                    'verified': {'type': 'boolean'},
                    'created_at': {'type': 'date'},
                    'description': {'type': 'text'},
                    'name': {'type': 'text'},
                    'id': {'type': 'long'},
                    'public_metrics': {
                        'properties': {
                            'following_count': {'type': 'long'},
                            'tweet_count': {'type': 'long'},
                            'listed_count': {'type': 'long'}
                        }
                    }
                }
            }
        }
    }
    settings = {
        'index': {
            'number_of_replicas': 2
        }
    }
    
    es = elastic(host)
    if es.indices.exists(index=index):
        es.indices.delete(index=index)
    if not es.indices.exists(index=index):
        es.indices.create(index=index, body={'settings': settings, 'mappings': mappings})
    
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--index")
    args = parser.parse_args()
    
    main(args.index)
