# Exploitation de tweets sur le changement climatique

Ceci est un projet d'exploitation de données issues de Twitter pour évaluer l'opinion publique sur le sujet du changement climatique. Comment est-il possible de se faire une idée de cette opinion (même restreinte aux utilisateurs de Twitter) en utilisant des outils à disposition sur le Datalab ? Avec `R` ou `Python`, on utilisera l'API de Twitter ainsi qu'Elasticsearch, un moteur de recherche distribué, particulièrement adapté à de la recherche textuelle libre, pour essayer de répondre à des questions telle que :
- Est-ce que les gens s'intéressent de plus en plus au changement climatique ?
- Y'a t-il des endroits où les gens s'intéressent plus au changement climatique ?
- Quelle est la proportion de climatosceptiques parmi les gens qui parlent du climat ?

## Mise en route

Sur le Datalab, pour récupérer le code, suivre une des indications suivantes :
- Depuis un service Rstudio : [Créer un projet Rstudio à partir d'un dépôt (8.4)](https://www.book.utilitr.org/git.html)
- Depuis un service Jupyter : Git -> Clone a Repository -> Entrer l'adresse https://git.lab.sspcloud.fr/ssplab/2022-06-funathon/climate-change-tweets.git
- Depuis l'un ou l'autre : Depuis un terminal, lancer la commande
```
git clone https://git.lab.sspcloud.fr/ssplab/2022-06-funathon/climate-change-tweets.git
```

Pour installer les librairies (Python) nécessaires et récupérer des données, lancer la commande suivante dans un Terminal à la racine du projet :
```
./setup.sh
```

Le code qui constitue une base pour se lancer se trouve dans les répertoires se trouvent dans les répertoires `notebooks` et `Rmd`. Dans l'ordre :
- `API Twitter et Elastic` est une introduction à l'utilisation de ces deux outils;
- `Changement climatique sur Twitter` illustre des requêtes à compléter pour commencer à faire une analyse des publications sur le changement climatique au sein de la plateforme Twitter.

Des informations supplémentaires sur Elasticsearch peuvent être trouvées dans le fichier `README.md` du projet https://git.lab.sspcloud.fr/ssplab/2022-06-funathon/siretiseretablissementspolluants.

## Ressources
- https://dataverse.harvard.edu/dataset.xhtml?persistentId=doi:10.7910/DVN/5QCCUU : données utilisées ici;
- https://twarc-project.readthedocs.io/en/latest/twarc2_en_us/ : librairie `twarc` qui fournit une interface avec l'API Twitter;
- https://docs.ropensci.org/rtweet/index.html : librairie `rtweet`;
- https://developer.twitter.com/en/docs/twitter-api/tweets/search/introduction : introduction à l'API Twitter;
- https://developer.twitter.com/en/docs/twitter-api/tweets/search/quick-start/recent-search : documentation sur la *recent search*;
- https://github.com/twitterdev/Twitter-API-v2-sample-code/blob/main/Full-Archive-Search/full-archive-search.py : documentation sur la *full archive search*;
- https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl.html : pour apprendre à faire des requêtes Elasticsearch.
