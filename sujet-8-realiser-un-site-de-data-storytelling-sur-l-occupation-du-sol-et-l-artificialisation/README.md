# Sujet 8 Réaliser un site de Data Storytelling sur l’occupation du sol et l’artificialisation

L’objectif du sujet est de construire et de raconter à partir de la base de données Teruti, qui propose une vision historique et territoriale de l’occupation du sol,  une histoire sous la forme d’un site web réalisé à l’aide de Quarto.

Un site explicatif est disponible à l'adresse : https://ssplab.pages.lab.sspcloud.fr/2022-06-funathon/sujet-8-realiser-un-site-de-data-storytelling-sur-l-occupation-du-sol-et-l-artificialisation/ 

Vous y trouverez :
- la présentation du sujet
- des exemples de pages de dataviz
- une documentation pour réaliser un site web Quarto
