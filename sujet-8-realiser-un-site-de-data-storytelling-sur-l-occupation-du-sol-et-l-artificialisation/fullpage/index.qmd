---
format:
  html:
    page-layout: custom  
    section-divs: false
    include-in-header:
      - header.html
    include-after-body: gestion_ancre.html
---

```{=html}
<style>
* {
  box-sizing: border-box;
}

.row {
  display: flex;
}

/* Create two equal columns that sits next to each other */
.column {
  flex: 50%;
  padding: 10px;
  height: 300px; 
}

/* Sections backgrounds
* --------------------------------------- *
* pour les sections avec un gradient de couleur*/

#section1{
  background-image: linear-gradient(128deg,#ffffff,#ffffff);
}

#section2{
	background-image: linear-gradient(128deg,#fc40ff,#543fff);
}

</style>
```



```{r}
#| label: library
#| include: false
#| warning: false
library(readr)
library(tidyr)
library(dplyr)
library(ggplot2)
library(gganimate)
#récupération du jeu de données des pingouins
library(palmerpenguins)
library(ggthemes)
```

<!-- Ajouter les images de fond de section ici -->

![](../images/sheep.jpg){style="display:none"} 
![](../images/planet.jpg){style="display:none"}

<!--  -->

::: {#fullpage}
::: {#section0 .sectionFullPage .active style="background-image:url('../images/planet.jpg')"}
# Data Storytelling

*Comment raconter efficacement une histoire avec des données*
:::

::: {#section1 .sectionFullPage}
::: {#slide1 .slide}
<!--diapo avec datawrapper-->

::: {style="padding-top: 20px;"}
<iframe title="Légère augmentation des ventes finales* des produits de l&#39;aquaculture en France depuis 2015, après de fortes baisses entre 1997 et 2015" aria-label="Interactive area chart" id="datawrapper-chart-FaG46" src="https://datawrapper.dwcdn.net/FaG46/12/" scrolling="no" frameborder="0" style="width: 0; min-width: 80% !important; border: none; ">

</iframe>

```{=html}
<script type="text/javascript">!function(){"use strict";window.addEventListener("message",(function(e){if(void 0!==e.data["datawrapper-height"]){var t=document.querySelectorAll("iframe");for(var a in e.data["datawrapper-height"])for(var r=0;r<t.length;r++){if(t[r].contentWindow===e.source)t[r].style.height=e.data["datawrapper-height"][a]+"px"}}}))}();
</script>
```
:::
:::

::: {#slide2 .slide style="background-image:url('../images/sheep.jpg')"}
## Un texte sur une image
:::
:::

::: {#section2 .sectionFullPage}
::: {#slide2Row .row}
::: {#slide2Col1 .column}
## Message à faire passer

*Un texte explicatif* : praesent magna orci, lobortis sit amet libero vel, maximus molestie dolor. Donec egestas non nulla nec scelerisque. Sed vel magna leo.

Pellentesque gravida augue quis neque porta, nec posuere justo vestibulum. Pellentesque gravida augue
:::

::: {#slide2Col2 .column}
```{r}
#| label: netflix
#| fig-cap: "Netflix film"
#| warning: false
#| echo: false
netflix_titles <- read_delim("../data/netflix_titles.csv", 
    delim = ";", escape_double = FALSE, locale = locale(), 
    trim_ws = TRUE)
grp_netflix <- netflix_titles %>% 
  group_by(release_year, type) %>% 
  count() %>% 
  filter(nchar(release_year)==4)



# grp_netflix %>%
#  filter(release_year >= 1991L & release_year <= 2020L) %>%
#  ggplot() +
#   aes(x = release_year, y = n, fill = type, colour = type) +
#   geom_area(size = 1.5) +
#   scale_fill_hue(direction = 1) +
#   scale_color_hue(direction = 1) +
#   theme_economist()

gif <- grp_netflix %>%
 filter(release_year >= 1991L & release_year <= 2020L) %>%
 ggplot() +
  aes(x = release_year, y = n, fill = type, colour = type) +
  geom_area(size = 1.5) +
  scale_fill_hue(direction = 1) +
  scale_color_hue(direction = 1) +
  theme_economist() +
  transition_reveal(release_year) +
  view_follow(fixed_y = TRUE)

anim_save(filename = "graph_netflix.gif", animation = gif, nframes = 100, end_pause = 10, rewind = TRUE,
          renderer = gifski_renderer(), path = "../images/")


```

![](../images/graph_netflix.gif)
:::
:::
:::

::: {#section3 .sectionFullPage}
Section Video <video id="myVideo" loop muted data-autoplay playsinline> <source src="../videos/Planet.mp4" type="video/mp4"> </video>

::: layerVideo
## Titre qui vient se mettre au dessus d'une video
:::
:::
:::
