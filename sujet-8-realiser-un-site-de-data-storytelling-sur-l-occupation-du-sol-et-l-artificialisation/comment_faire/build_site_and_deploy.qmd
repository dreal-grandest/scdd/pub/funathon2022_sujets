---
title: "Construire le site web et le déployer sur Gitlab"
format:
  html:
    toc: true
    toc-location: right
    toc-title: "Sommaire"
    code-fold: true
    code-summary: "Voir le code"
    code-copy: true
---

## Construire le site web

La construction du site web se fait simplement dans RStudio via le menu ***Build***, puis ***Render Website*** (ou le raccourci clavier CTRL + Shift + B). Tous les fichiers .qmd sont transformés en .html et transférées dans le répertoire ***public***. Les images, fichiers css, vidéos... sont également transférés dans ***public.*** Le site est consultable directement dans l'onglet Viewer de Rstudio et peut être vu en grand directement dans le navigateur grâce au bouton "Show in a new window".

## Déployer le site sur Gitlab

Si le site construit est conforme aux attentes, il peut être déployé sur Gitlab :

1.  Faire un commit avec tous les fichiers modifiés

2.  Faire un push pour envoyer tous les fichiers sur Gitlab

3.  Option : si vous travaillez sur des branches, il faut les merger dans le "main"

4.  Automatiquement le site est publié sur Pages grâce à la fonction CI/CD de Gitlab et le fichier .gitlab-ci.yml

L'accès à Pages est visible sur Gitlab --\> Settings :

![](/images/gitlab_settings.png)

Puis cliquer sur Pages :

![](/images/gitlab_pages.png)

Pour vérifier si le nouveau site a bien été déployé il faut aller dans Gitlab --\> CI/CD

![](/images/gitlab_ci_cd.png)

Puis cliquer sur Pipelines :

![](/images/gitlab_pipes.png)
