# Siretiser les Etablissements Polluants

En mobilisant la technologie ElasticSearch, siretiser les établissements du registre français des émissions polluantes

1. Pourquoi Elasticsearch?
2. Présentation générale et vocabulaire
3. Connexion au moteur Elastic mis en place pour le funathon
4. Accéder au code
5. Structure du dépôt

## 1. Pourquoi Elasticsearch? 

**Elasticsearch** est un _moteur de recherche et d'analyse_ dans un ensemble de documents particulièrement performant sur les données textuelles, ou pour toute requête structurée comprenant des données numériques, textuelles, géospatiales. Les résultats de la recherche sont classés automatiquement par pertinence. Les recherches peuvent être hautement flexibles et bénéficier d'un utilisateur métier expert qui saurait comment la spécifier.  

Dans le cadre particulier de l'identification des entreprises, Elasticsearch fait partie de la solution retenue par
* l'API "Sirene données ouvertes" (DINUM) (cf https://annuaire-entreprises.data.gouv.fr/) 
* l'API de recherche d'entreprises Française de la Fabrique numérique des Ministères Sociaux (cf https://api.recherche-entreprises.fabrique.social.gouv.fr/)
* le projet de l'Insee "Amélioration de l'identification de l'employeur dans le recensement", pour faire une première sélection des établissements pertinents pour un bulletin de recensement donné. 

Dans le cadre de l'identification des individus, Elasticsearch fait partie de la solution envisagée pour l'identification des individus au RNIPP (Répertoire national des personnes physiques) pour le projet CSNS (Code statistique non signifiant), et est la solution technique sous-jacente au projet [matchID](https://matchid.io/) du ministère de l'intérieur.

C'est également un outil qui peut être utilisé pour des appariements flous ad-hoc dans le cadre d'étude à l'Insee, par exemple au niveau produits entre les données de caisse de RelevanC et OpenFoodFacts et entre les points de ventes RelevanC et Sirius [[Communication JMS](http://jms-insee.fr/jms2022s28_2/)].

Une introduction à Elastic Search pour l'appariement flou est disponible sur le datalab ici : [Notebook d'introduction](https://www.sspcloud.fr/formation?search=&path=%5B%22Analyse%20Textuelle%22%5D)

Au delà du secteur public, on peut citer qu'Amazon AWS fait partie des utilisateurs historiques d'Elasticsearch. 

### 2. Présentation générale et vocabulaire 


Un **index**  est une collection de **documents** dans lesquels on souhaite chercher, préalablement ingérés dans un moteur de recherche Elasticsearch (étape d'indexation), dans notre cas d'usage, les documents sont les établissements. **L'étape d'indexation a été faites préalablement dans un moteur mis à disposition sur le datalab à tous.**   L'indexation consiste à pré-réaliser les traitements des termes des documents pour gagner en efficacité lors de la phase de recherche. L'indexation est faites une fois pour de nombreuses recherches potentielles, pour lesquelles la rapidité de réponse peut être crutiale.

Les documents sont constitués de variables, les **champs** ('fields'), dont le **type** est spécifié ("text", "keywoard", "geo_point", "numeric"...) à l'indexation.

Les **analyzers** sont très utiles pour les données textuelles: ils se composent en général d'un **tokenizer** (méthode pour séparer le texte en éléments unitaires, les tokens, en général des mots, mais cela peut aussi être des n-grammes de caractères) et de **filtres**, par exemple de certains mots (stopwords, gestion des synonymes). Chaque champ peut être associé à un analyzer particulier, dans un objet défini à l'indexation, le **mapping**. Le mapping comporte le schéma des données ainsi que la façon dont ils seront analysés pour la recherche, i.e. champ, type et analyzer associé.

L'utilisateur va requêter le moteur de recherche via des **query**. Ces dernières sont très flexibles et constitue un langage en soi: on parle de Query DSL (Domain-Specific Language). 

La documentation Elastic va répondre à au moins trois enjeux, à savoir distinguer pour ne pas s'y perdre:

1. Mettre en place et configurer un cluster Elastic Search adapté au besoin 
2. Indexer intelligemment ses données: définir les types et les analyzers 
3. Requêter intelligemment ces données 

L'étape 1 a été gérée par la DIIT via la mise en place de services Elastic Search sur le datalab. L'étape 2 a été gérée par le SSP Lab, en reprenant pour le funathon l'indexation utilisée dans le cadre du projet AIEE "Amélioration de l'identification de l'employeur dans le recensement". Le sujet peut donc être traité intégralement en s'intéressant à la dernière étape. 

Les mots clés utiles pour parcourir la documentation sont "Query DSL", "Text analysis", "Search data". 
https://www.elastic.co/guide/en/elasticsearch/reference/current/index.html 

Il peut être utile de parcourir le [Cours ensae Python](https://pythonds.linogaliana.fr/elastic/), bien qu'il y ait quelques redondances avec la suite de ce notebook sur la partie "requête", la partie indexation y est abordée.


### 3. Connexion au moteur Elastic mis en place pour le funathon 

ElasticSearch est installé sur un serveur (ici le datalab) qu'il est possible de requêter depuis un client, par exemple une session R ou Python du datalab, ou encore, l'interface graphique associée à ElasticSearch nommée Kibana. Cette dernière est pratique
pour tester des requêtes mais elle n'a pas été rendue disponible pour chaque participant à l'occasion du funathon. En revanche, pour aller plus loin, il est possible d'ouvrir son propre service ElasticSearch via le datalab (étape 1), d'y indexer ses propres données (étape 2), et d'avoir accès à l'interface Kibana. 


### 4. Accèder au code

1. Depuis un service Rstudio 

[Créer un projet Rstudio à partir d'un dépôt (8.4)](https://www.book.utilitr.org/git.html)
`sujet_4.Rmd`


2. Depuis un service Jupyter

Par exemple: Git -> Clone -> Entrer l'adresse https://git.lab.sspcloud.fr/ssplab/2022-06-funathon/siretiseretablissementspolluants.git

Démarrer avec: `sujet_4_notebook_python.ipynb`(python) ou `sujet_4_notebook_R.ipynb` (R)


3. Depuis l'un ou l'autre

Depuis un terminal

```
git clone https://git.lab.sspcloud.fr/ssplab/2022-06-funathon/siretiseretablissementspolluants.git
```


