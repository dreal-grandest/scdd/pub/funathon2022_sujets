# Sujet 3: Une maison individuelle est-elle écologique ? Où sont situées les passoires thermiques en France ?

## Les données utilisées

Pour apporter des éléments de réponse au sujet nous allons utiliser les données open-source de l'Ademe sur les "Diagnostics de performance énergétique (DPE) pour les logements par habitation" : https://data.ademe.fr/datasets/dpe-france
La base unifiée au niveau France entière n'est cependant pas téléchargeable facilement et immédiatemment sur le site.

Pour les curieux, une autre base disponible au niveau bâtiment existe, mais elle n'a pas été regardée par l'équipe Funathon : la Base de Données Nationale des Batiments
https://gitlab.com/BDNB/base_nationale_batiment

## Les différentes pistes possibles, avec les compétences à utiliser/apprendre

Ce sujet ne nécessite pas de compétences et techniques particulières, il s'agit essentiellement de manipulation de bases de données et de data visualisation. Un sujet plutôt recommandé à ceux souhaitant s'exercer à utiliser R (si les souvenirs sont lointains ou naissants !), découvrir la représentation géographique, ou mettre les mains pour la première fois sur Python. Une partie facultative du sujet aborde également l'utilisation d'une API pour récupérer les données DPE.

Le champ des possibles est beaucoup plus large que les étapes proposées dans ce sujet, des structures entières sont fondées sur ces données : https://namr.com/fr/

La 1ère étape est facultative, et si vous n'êtes pas intéressés, la base unifiée France entière (extraite au 1/10e) est directement disponible sur l'espace de stockage de la plateforme (espace nommé "minio")
- 1ère étape (facultative): intéragir avec l'application/API de l'Ademe pour télécharger les données -> réaliser des requêtes via l'API pour constituer une base unique
- 2ème étape: premières explorations de la base et caractéristiques descriptives des logements écologiques (type de maison, année de construction, surface, etc) -> R: `dplyr` & `ggplot` / Python: `pandas` & `seaborn`, des exemples à reproduire peuvent être la publication du SDES sur le sujet : https://www.statistiques.developpement-durable.gouv.fr/sites/default/files/2021-12/document_travail_49_parc_logements_consommation_energie_septembre2020.pdf
- 3ème étape: représentation spatiale des logements -> R: `sf` / Python: `geopandas`, éventuellement comparer à la répartition géographique des dossiers déposés pour MaPrimeRenov pour juger d'un éventuel effet
- 4ème étape : pour aller plus loin - notebook non fournis - il est possible de prédire la DPE en fonction des caractéristiques du logement (et en 5ème étape identifier les fraudes aux DPE ?), réaliser une application interactive `Rshiny` ou `streamlit` pour visualiser les résultats, et bien d'autres idées possibles et immaginables.

## Comment s'y prendre pour le Funathon ?

Pour travailler sur ce sujet il y a différentes manières de procéder, du parcours le plus accompagné au plus libre :
- Facile : suivre pas à pas les notebooks des parties en relançant le code, en comprenant ce qui est utilisé et en **l'adaptant à de nouvelles idées de graphiques**
- Intermédiaire : essayer de répondre aux questions en faisant soit même le code et/ou en s'appuyant de temps en temps sur les notebooks.. Attention à ne pas avoir recours aux notebooks trop systématiquement, l'apprentissage passe **justement** par des résolutions de bugs et d'échecs en s'obstinant avec l'aide de Stack Overflow ou de son équipe !
- Difficile : répondre aux questions sans aucune aide, en partant de zéro (fonds de cartes disponibles sur Minio tout de même)
- Advanced : Vous trouvez les notebooks et techniques triviaux mais êtes intéressés pour exploiter les DPE ? Trouvez-vous même des questions auxquelles répondre en parcourant les bases ! Ne vous limitez pas aux stats descriptives proposées mais faites des appariments, créer un Rshiny, appliquez des modèles de machine learning pour prédire les DPE (la 4ème étape proposée précédemment) !
