Au cours du premier trimestre 2019 s'est tenu un _grand débat national_ en France. 4 thèmes étaient proposés par le gouvernement : transition écologique, fiscalité et dépenses publiques, démocratie et citoyenneté, organisation de l'état et des services publics. Ce débat a pris plusieurs formes, mais l'une d'elle était la possibilité de donner son avis via l'utilisation d'un formulaire Internet.

L'idée de ce sujet est d'analyser le texte brut des contributions collectées - accessibles directement sur le site du grand débat - pour comprendre ce que proposent le sujet sur le premier thème, également celui du Funathon : la transition écologique.

1. `01_analyse_et_nettoyage_questions_ouvertes.Rmd`
**Objectif:** Première analyse et normalisation du texte

2. `02_preoccupations_environnementales_et_expositions_locales.Rmd`
**Objectif:** Les contributeurs se déclarant particulièrement préoccupés par un risque environnemental donné (par exemple la pollution de l'air) sont-ils particulièrement concernés dans leur environnement "immédiat"?

3. `03_LDA_clustering_de_reponses.Rmd`
**Objectif:** Appliquer une méthode de clustering aux réponses à une question du grand débat.
