# Initiation à `Git` dans le cadre du `Funathon`

__Déplacé le 6 octobre 2022 dans https://gitlab-forge.din.developpement-durable.gouv.fr/pub/dreal-grandest/funathon2022_sujets--> https://gitlab-forge.din.developpement-durable.gouv.fr/dreal-grandest/scdd/pub/funathon2022_sujets__

__A FAIRE: Une fois le feu vert de migration envoyé à la Dnum pour changer l'architecture de dépôt sur la Forge GitLab du MTE : Changer la visibilité Interne --> public__

## Mise à disposition des supports des sujets

* [Sujet 1 - GrandDebat (maj 20/06/2022 07:50)](https://git.lab.sspcloud.fr/olivierguerin/sujet-0-initiation-git/-/tree/master/granddebat)

* [Sujet 2 - Twitter et changement climatique (maj 20/06/2022 07:50)](https://git.lab.sspcloud.fr/olivierguerin/sujet-0-initiation-git/-/tree/master/climate-change-tweets)

* [Sujet 3 - Diagnostic de performance énergétique des logements (maj 20/06/2022 07:50)](https://git.lab.sspcloud.fr/olivierguerin/sujet-0-initiation-git/-/tree/master/diagnosticperformanceenergetique)

* [Sujet 4 - SiretiserEtablissementsPolluants (maj 20/06/2022 07:50)](https://git.lab.sspcloud.fr/olivierguerin/sujet-0-initiation-git/-/tree/master/siretiseretablissementspolluants)

* [Sujet 5 - Prévoir la pollution avec des données météo (maj 20/06/2022 07:50)](https://git.lab.sspcloud.fr/olivierguerin/sujet-0-initiation-git/-/tree/master/sujet-5-prevoir-la-pollution-avec-des-donnees-meteo)

* [Sujet 6 - Quel impact de la montée des eaux sur la population française ? (maj 20/06/2022 07:50)](https://git.lab.sspcloud.fr/olivierguerin/sujet-0-initiation-git/-/tree/master/sujet-6-montee-des-eaux)

* [Sujet 7 - Classer les réponses du Grand Débat National en grands domaines (maj 20/06/2022 07:50)](https://git.lab.sspcloud.fr/olivierguerin/sujet-0-initiation-git/-/tree/master/sujet-7-classer-les-reponses-du-grand-debat-dans-des-categories-predefinies)

* [Sujet 8 - Réaliser un site de Data Storytelling sur l'oocupation du sol et l'artificialisation (maj 20/06/2022 07:50)](https://git.lab.sspcloud.fr/olivierguerin/sujet-0-initiation-git/-/tree/master/sujet-8-realiser-un-site-de-data-storytelling-sur-l-occupation-du-sol-et-l-artificialisation)

* [Sujet 9 - Prédire l’occupation du sol à partir d’images satellites (maj 20/06/2022 07:50)](https://git.lab.sspcloud.fr/olivierguerin/sujet-0-initiation-git/-/tree/master/sujet-9-detecter-la-couverture-du-sol-a-partir-d-images-satellites)

Présentation du 16/06/2022 par les 2 collègues du SSPLAB 

## Objectif

- Découvrir `Git` à travers l'interface `RStudio`
- Découvrir l'interaction entre `Git`, `RStudio` et `Gitlab`
- Se familiariser avec quelques concepts de `Git`

## Méthode préconisée

On propose de travailler sur la même branche mais des fichiers
séparés. Cela constitue une initiation plus simple à `Git`
que l'utilisation de branches tout en évitant les conflits. 

## Fil conducteur

### Configuration

- Créer un compte Gitlab
- Se connecter au SSPCloud
- Créer un jeton à conserver quelque part (proposition: dans l'onglet `Mon Compte`)
- Chaque membre du groupe va sur la page d'accueil du projet
- Une des personnes doit forker le dépôt, les autres attendent
- La personne qui a fork ouvre les droits aux autres
- Retour sur la page d'accueil

### Récupération du dépôt

- Ouvrir un service RStudio sur SSPCloud
- Récupérer l'url pour être en mesure de cloner
- Cloner en utilisant le clique bouton de RStudio
- Découverte de l'onglet `Git`en haut à droite
- Passer en ligne de commande

```shell
git config --global credential.helper 'cache --timeout=200000'
```

### Premier commit

- Chacun crée un fichier `prenom-nom.md`
- Ecrire une phrase dedans
- Le fichier apparaît à droite avec statut `?`
- Cliquer sur la boite. Le statut doit changer pour devenir `A` --> `git add`
- Appuyer au dessus sur `commit`, une fenêtre s'ouvre (l'autoriser si le navigateur bloque).
Observer le changement
- Valider en mettant un message à droite et en cliquant sur `Commit` --> premier commit

### Interaction avec le dépot

- Appuyer sur la fleche verte (`push`)
- Il est nécessaire de s'authentifier une fois pour confirmer l'identité de notre
dépôt local


A ce stade:

- Personne la plus rapide: message qui indique un push réussi
- Autres: `push` refusé. C'est normal, il faut être à jour avec le dépôt pour
pouvoir pousser ses modifications. 

On va donc récupérer en local les dernières modifications

- Appuyer sur la fleche bleue (`pull`) pour récupérer les modifications
- Tenter à nouveau le `push` (flèche verte)
- Refaire ça si vous êtes plus de deux
